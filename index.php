<?php

require __DIR__ . '/vendor/autoload.php';

error_reporting(E_ALL);
ini_set( 'display_errors', 1 );

// ロギング用のライブラリ
use \Monolog\Logger;
use \Monolog\Handler\StreamHandler;

// HTTP通信用のライブラリ
use \GuzzleHttp\Client;

$app = new \Slim\App(
    new \Slim\Container([
        'settings' => [
            'displayErrorDetails' => true,
            'fb' => [
                'app_id' => '', // あとで書く
                'access_token' => '', // あとで書く
                'page_id' => '', // あとで書く
                'endpoint' => 'https://graph.facebook.com/v2.6/me/messages'
            ]
        ]
    ])
);

// $container
$c = $app->getContainer();

// logger
$c['logger'] = function($c) {
    $logger = new Logger('BOT_LOG');
    $logger->pushHandler(new StreamHandler('php://stderr', Logger::DEBUG));
    return $logger;
};

// httpClient
$c['client'] = function($c) {
    $client = new Client([
        'base_uri' => $c['settings']['fb']['endpoint'],
    ]);
    return $client;
};

// view
$c['view'] = function($c) {
    $view = new \Slim\Views\Twig(
        realpath(__DIR__ . '/app/templates'),
        [
            'cache' => '/tmp/cache',
            'debug' => true
        ]
    );
    $basePath = str_ireplace('index.php', '', $c['request']->getUri()->getBasePath());
    $basePath = rtrim($basePath, '/');
    $view->addExtension(
        new \Slim\Views\TwigExtension($c['router'], $basePath)
    );

    $twig = $view->getEnvironment();

    $view['script'] = 'script';
    return $view;
};

/**
 * Routing
 */
/**
 * indexページ
 */
$app->get('/', function($req, $res) {
    phpinfo();
});

/**
 * setup a webhook
 *
 * @see https://developers.facebook.com/docs/messenger-platform/implementation#setup_webhook
 */
$app->get('/callback', function($req, $res) {
    $token = ''; // あとで書く！
    $verifyToken = $req->getParam('hub_verify_token');
    $challenge = $req->getParam('hub_challenge');

    if ($token === $verifyToken) {
        return $res->write($challenge);
    }

    $this->logger->addInfo($challenge);
    return $res->withStatus(400)
        ->write('Error');
});

/**
 * メッセージ受信
 *
 * @see https://developers.facebook.com/docs/messenger-platform/implementation#receive_message
 */
$app->post('/callback', function($req, $res) {
    // ここでロギングしてるよー
    $message = $req->getBody();
    $this->logger->addInfo($message);

    $body = json_decode($message, true);
    $entry = $body['entry'][0];
    $this->logger->addInfo(json_encode($entry));

    $user = $entry['messaging'][0]['sender']['id'];

    /*
     * メッセージ送信の基本形
     */
    $reply = [
        'recipient' => [
            'id' => $user
        ],
        'message' => [
            'text' => '心のこもったメッセージ'
        ]
    ];


    $fb = $this['settings']['fb'];
    $client = $this['client'];
    $response = $client->request('POST', '', [
        'query' => [
            'access_token' => $fb['access_token'],
        ],
        'json' => $reply
    ]);
});

/**
 * Subscription
 *
 * @see https://developers.facebook.com/docs/messenger-platform/implementation#subscribe_app_pages
 */
$app->get('/subscription', function($req, $res) {
    $view = $this->view;
    $view->render(
        $res,
        'subscription.twig'
    );
})->setName('subscription');

$app->post('/subscription', function($req, $res) {
    $fbConfig = $this->settings['fb'];

    $client = new Client([
        'base_uri' => 'https://graph.facebook.com/v2.6/me/subscribed_apps'
    ]);

    $result = $client->request('POST', '', [
        'query' => ['access_token' => $fbConfig['access_token']]
    ]);

    $res = $res->withHeader('Content-Type', 'application/json');
    return $res->write($result->getBody());
});

$app->run();
